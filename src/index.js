import React from 'react';
import ReactDOM from 'react-dom';
import { MainRouter } from './components/router/MainRouter';
import './index.css';

const rootElem = document.getElementById('root');
ReactDOM.render(<MainRouter />,rootElem);
