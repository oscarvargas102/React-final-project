import React from "react";
import { BrowserRouter as Router, Route, Switch, Link, useParams } from 'react-router-dom';
import { PersistGate } from 'redux-persist/es/integration/react';
import { Home } from '../views/Home/Home';
import { Footer } from '../commons/Footer/Footer';
import { ArtistsListView } from '../views/ArtistsListView/ArtistsListView';
import { ArtistView } from '../views/ArtistView/ArtistView';
import { AlbumView } from '../views/AlbumView/AlbumView';
import { Provider } from "react-redux";
import createStore from './../../state/store/configureStore'


export const MainRouter = () => {
    const { persistor, store } = createStore();
    return (
        <div>
            <Provider store={store}>
                <PersistGate persistor={persistor}>
                    <Router>
                        <header>
                            <nav>
                                <ul>
                                    <li><Link to='/'>Home</Link></li>
                                    <li><Link to='/artistsList/:searchedTxt'>Artists</Link></li>
                                </ul>
                            </nav>
                        </header>   
                        <Switch>
                            <Route exact path="/" component={Home} />
                            <Route path="/artistsList/:searchedTxt" component={ArtistsListView} />
                            <Route exact path="/artists/:searchedTxt" component={ArtistView} />
                            <Route exact path="/artists/:artist/:album" component={AlbumView} />
                        </Switch>
                    </Router>
                </PersistGate>
            </Provider>
            <Footer />
        </div>
    )
}