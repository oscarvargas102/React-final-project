import React from 'react';
import { useSelector } from 'react-redux';
import { TracksList } from './../../commons/TracksList/TracksList';
import noImage from './../../../assets/NO_IMG_600x600.png';

export const AlbumView = (props) => {
    const { currentAlbum, currentArtist, tracksItems, isLoading } = useSelector(store => store.componentDataReducer);

    const resultContainer = isLoading ? <div>Is loading...</div> : <TracksList dataItems={tracksItems} />;
    const logo =currentAlbum.images.filter(image => image.height === 640);
    const srcImg = logo == false ? noImage : logo[0].url;

    return (
        <div>
            Album View
            <div className="data-box">
                <div className="data-box__image">
                    <img src={srcImg} alt={currentAlbum.title} />
                </div>
                <div className="data-box__info">
                    <div>{currentAlbum.name}</div>
                    <div>{currentArtist.name} - {currentAlbum.release_date.slice(0, 4)}</div>
                </div>
            </div>
            {resultContainer}
        </div>
    )
}