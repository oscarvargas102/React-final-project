import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Header } from './../../commons/Header/Header';
import { getArtists } from './../../../state/actions/dataActions';
import { Searcher } from './../../commons/Searcher/Searcher';
import { DataBoxList } from '../../commons/DataBoxList/DataBoxList';


export const Home = () => {
    const { searchedText, favoriteSongs} = useSelector(store => store.componentDataReducer);
    console.log(favoriteSongs);
    
    const renderFav = favoriteSongs ? (
                <div>
            <div>Favorites</div>
            <DataBoxList dataItems={favoriteSongs} />
        </div>
    )
    : (
        null
    )
    return (
        <React.Fragment>
            <Header />
            <div className="container">
                <h3>Welcome to</h3>
                <h1>Spotisearch</h1>
                <p>Search your favorite songs over Spotify, just enter an artist's name in the following search box and enjoy!</p>
                <Searcher />
                <Link to={`/artistsList/${searchedText}`}>
                    <button type="button">Find</button>
                </Link>  
                {renderFav}
            </div>
        </React.Fragment>
    )
}