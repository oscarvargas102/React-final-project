import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { makeArtistsSearch, searchArtistAlbums, saveCurrentArtist } from '../../../state/actions/dataActions';
import { Header } from '../../commons/Header/Header';
import { Searcher } from '../../commons/Searcher/Searcher';
import { DataBoxList } from '../../commons/DataBoxList/DataBoxList';

export const ArtistsListView = (props) => {
    const dispatch = useDispatch();
    const { searchedText, isLoading, artistsItems} = useSelector(store => store.componentDataReducer);
    const { match: {params}} = props;
    const handleClick = artistInfo => {
        dispatch(saveCurrentArtist(artistInfo));
        dispatch(searchArtistAlbums(artistInfo.id));
    }
    
    useEffect(() => {
        dispatch(makeArtistsSearch(searchedText))
    }, [searchedText]);

    const resultContainer = isLoading ? <div>Is loading...</div> : <DataBoxList dataItems={artistsItems} onClick={handleClick}/>;

    return (
        <React.Fragment>
            <Header />
            <div className="container">
                <h1>Artist</h1>
                <h3>{`You are currently searching: "${searchedText}"`}</h3>
                <Searcher />
                <Link to={`/artistsList/${searchedText}`}>
                    <button type="button">Find</button>
                </Link>  
            </div>
            {resultContainer}
    </React.Fragment>

    )
}