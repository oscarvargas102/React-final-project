import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { saveCurrentAlbum, searchTracks } from '../../../state/actions/dataActions';
import { DataBoxList } from './../../commons/DataBoxList/DataBoxList';
import noImage from './../../../assets/NO_IMG_600x600.png';


export const ArtistView = (props) => {
    const dispatch = useDispatch();
    const { currentArtist, albumsItems, isLoading } = useSelector(store => store.componentDataReducer);

    const handleClick = (albumInfo) => {
        console.log(albumInfo)
        dispatch(saveCurrentAlbum(albumInfo));
        dispatch(searchTracks(albumInfo.id));
    }
 

    const resultContainer = isLoading ? <div>Is loading...</div> : <DataBoxList dataItems={albumsItems} onClick={handleClick}/>;

    const logo =currentArtist.images.filter(image => image.height === 640);

    const srcImg = logo == false ? noImage : logo[0].url;

    return (
        <div>
            Artist View
            <div className="data-box">
                <div className="data-box__image">
                    <img src={srcImg} alt={currentArtist.name} />
                </div>
                <div className="data-box__info">
                    <div>{currentArtist.name}</div>
                    <div>{currentArtist.genres.join(' - ')}</div>
                </div>
            </div>
            {resultContainer}
        </div>
    )
}