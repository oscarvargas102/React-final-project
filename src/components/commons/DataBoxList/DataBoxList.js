import React from 'react';
import { DataBox } from './../DataBox/DataBox';
import './dataBoxList.scss';

export const DataBoxList = (props) => {
    const {dataItems} = props;
    return (
        <div className="box-list">
            <div className="box-list__container">
                {dataItems.map((item) => {
                    return <DataBox 
                        key={item.id}
                        itemData={item} 
                        onClick={props.onClick}
                    />
                })}
            </div>
        </div>
    )
}