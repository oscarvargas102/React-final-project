import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import noImage from './../../../assets/NO_IMG_600x600.png';
import './dataBox.scss';

export const DataBox = (props) => {
    const { currentArtist} = useSelector(store => store.componentDataReducer);
    
    const { itemData } = {...props}
    const redirectUrl = itemData.album_type ? `/artists/${currentArtist.name}/${itemData.name}` : `/artists/${itemData.name}`;
    const logo =itemData.images.filter(image => image.height === 640);
    const srcImg = logo == false ? noImage : logo[0].url;
    
    return (
        <Link className="data-box" to={redirectUrl} onClick={() => props.onClick(itemData)}>
                <div className="data-box__image">
                    <img src={srcImg} alt={props.title} />
                </div>
                <div className="data-box__info">
                    <div>{itemData.name}</div>
                </div>
        </Link>
    )
}