import React from 'react';
import { CdBox } from './../CdBox/CdBox';
import './trackList.scss';

export const TracksList = (props) => {
    const {dataItems} = props;

    const groupByDisc = dataItems.reduce((acc, curr) => {
        if(!acc[curr.disc_number]) acc[curr.disc_number] = [];
        acc[curr.disc_number].push(curr);
        return acc;
    }, {})
    
    return (
        <div className="box-list">
            <div className="box-list__container">
                {Object.entries(groupByDisc).map((disc, index) => {
                    return (
                        <div className="tracks-table">
                            <div className="tracks-table__header">{`CD ${index + 1}`}</div>
                            <CdBox tracks={disc[1]} />
                        </div>
                    )
                })}
            </div>
        </div>
    )
}