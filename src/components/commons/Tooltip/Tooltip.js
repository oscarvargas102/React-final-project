import React from 'react';
import './tooltip.scss'

export const Tooltip = (props) => {
    return (
        <div className="tooltip">
            {props.text}
        </div>
    )

}