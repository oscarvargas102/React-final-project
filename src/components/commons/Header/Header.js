import React from 'react';
import './header.scss';
import spotifyLogo from './../../../assets/Spotify_Logo_RGB_Green.png';

export const Header = (props) => {
    return (
        <div className="header-container">
            <img src={spotifyLogo} alt="Logo" />
            {props.children}
        </div>
    )
}