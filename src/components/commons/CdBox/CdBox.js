import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Tooltip } from './../Tooltip/Tooltip';
import './cdBox.scss';
import { addFavoriteSong, removeFavoriteSong } from '../../../state/actions/dataActions';

export const CdBox = (props) => {
    const dispatch = useDispatch();
    const { currentArtist, currentAlbum, favoriteSongs} = useSelector(store => store.componentDataReducer);
    const handleClick = (item) =>  {
        const matchSong = favoriteSongs.find(song => song.id === item.id)
   
        if (matchSong) {
            dispatch(removeFavoriteSong(item.id));
        }
        else {
            const songInfo = {
                id: item.id,
                name: item.name,
                albumName: currentAlbum.name,
                images: currentAlbum.images,
                title: currentArtist.name
            }            
            dispatch(addFavoriteSong(songInfo));
        }        
    }
    
    const handleEnter = e => {
        e.preventDefault();
        e.target.nextSibling.classList.add('tooltip--active');
    }

    const handleLeave = e => {
        e.preventDefault();
        e.target.nextSibling.classList.remove('tooltip--active');
    }
    
    
    const { tracks } = {...props}
    
    return (
        <React.Fragment>
            {tracks.map((item) => {
                const songClass = favoriteSongs.find(song => song.id === item.id) ? 'star--filled' : '';
                const tooltipTxt = songClass ? 'Remove from favorites' : 'Add to favorites';
                return (
                    <div className="tracks-table__row">
                        {item.name}<span className={`star ${songClass}`} onMouseOut={handleLeave} onMouseOver={handleEnter} onClick={() => handleClick(item)}>☆</span>
                        <Tooltip text={tooltipTxt} />
                    </div>
                ) 
            })}
        </React.Fragment>
        )

}