import React, { useRef } from 'react';
import { useDispatch } from 'react-redux';
import { saveQuery } from './../../../state/actions/dataActions';
import './searcher.scss';

export const Searcher = () => {
    const textInput = useRef();
    const dispatch = useDispatch();

    const handleSearch = (e) => {
        e.preventDefault();
        console.log('enter click', e.target.value)
        //setQuery(e.target.value)
        dispatch(saveQuery(e.target.value))
    }
    
    return (
        <div className="searcher-container">            
            <input 
                type="text"
                ref={textInput}
                required 
                placeholder="Search for your artist here" 
                onBlur={handleSearch}
                defaultValue=''
            />
        </div>
    )
};