import {
    CURRENT_SEARCH,
    DATA_FETCHING,
    ARTISTS_LIST_SUCCESS,
    ARTIST_SUCCESS,
    SAVE_CURRENT_ARTIST,
    SAVE_CURRENT_ALBUM,
    TRACKS_SUCCESS,
    ADD_SONG,
    REMOVE_SONG
} from './../constants/index';

const initialState = {
    artistsItems: [],
    albumsItems: [],
    tracksItems: [],
    isLoading: false,
    searchedText: '',
    currentArtist: {},
    currentAlbum: {},
    favoriteSongs: []
}


export const componentDataReducer = (state = initialState, action) => {
    const {type, payload} = action;
    switch (type) {
        case CURRENT_SEARCH: 
            return {
                ...state,
                searchedText: payload, 
                isLoading: false,
            }
        case DATA_FETCHING:
            return {
                ...state,
                isLoading: true
            }
        case ARTISTS_LIST_SUCCESS:
            console.log('******************', payload);
            return {
                ...state,
                artistsItems: payload,
                isLoading: false
            }
        case ARTIST_SUCCESS:
            return {
                ...state,
                albumsItems: payload,
                isLoading: false
            }
        case TRACKS_SUCCESS:
            return {
                ...state,
                tracksItems: payload,
                isLoading: false
            }
        case SAVE_CURRENT_ARTIST:
            return {
                ...state,
                currentArtist: payload
            }
        case SAVE_CURRENT_ALBUM:
            console.log('***', payload);
            return {
                ...state,
                currentAlbum: payload
            }
        case ADD_SONG:
            console.log( state)
            return {
                ...state,
                favoriteSongs: [...state.favoriteSongs, payload]
            }
        case REMOVE_SONG:
            console.log( state)
            return {
                ...state,
                favoriteSongs: state.favoriteSongs.filter(item => item.id !== payload)
            }
        default:
            return state;
    }
}