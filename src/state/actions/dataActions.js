import { apiUrl, accessToken } from './../services/config';
import axios from 'axios';
import { 
    CURRENT_SEARCH,
    DATA_FETCHING,
    ARTISTS_LIST_SUCCESS,
    ARTIST_SUCCESS,
    SAVE_CURRENT_ARTIST,
    SAVE_CURRENT_ALBUM,
    TRACKS_SUCCESS,
    ADD_SONG,
    REMOVE_SONG
} from './../constants/index';

export const saveCurrentArtist = artistInfo => dispatch => {
    dispatch({type: SAVE_CURRENT_ARTIST, payload: artistInfo});
}

export const saveCurrentAlbum = albumInfo => dispatch =>{
    console.log(albumInfo)
    dispatch({type: SAVE_CURRENT_ALBUM, payload: albumInfo});
}

export const makeArtistsSearch = query => async dispatch => {
    try {
        // status loading
        dispatch({ type: DATA_FETCHING });
        await axios({
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
            },
            url: `${apiUrl}search?q=${query}&type=artist`
        }).then(response => {
            dispatch({ type: ARTISTS_LIST_SUCCESS, payload: response.data.artists.items });
        }).catch(response => {
            console.log('catch', response)
        })
    } catch (err) {
        console.log('err', err)
    }
};

export const searchArtistAlbums = id => async dispatch => {
    try {
        // status loading
        dispatch({ type: DATA_FETCHING });
        await axios({
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
            },
            url: `${apiUrl}artists/${id}/albums`
        }).then(response => {
            console.log(response);
            
           dispatch({ type: ARTIST_SUCCESS, payload: response.data.items });
        }).catch(response => {
            console.log('catch', response)
        })
    } catch (err) {
        console.log('err', err)
    }
};

export const searchTracks = id => async dispatch => {
    try {
        // status loading
        dispatch({ type: DATA_FETCHING });
        await axios({
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
            },
            url: `${apiUrl}albums/${id}/tracks`
        }).then(response => {
            console.log(response);
            
           dispatch({ type: TRACKS_SUCCESS, payload: response.data.items });
        }).catch(response => {
            console.log('catch', response)
        })
    } catch (err) {
        console.log('err', err)
    }
};


export const saveQuery = query => async dispatch => {
    try {
        console.log('thunk call');
    
        dispatch({
            type: CURRENT_SEARCH,
            payload: query
        });
    
    } catch (err) {
      console.log(err);
    }
};

export const addFavoriteSong = songInfo => dispatch => {
    dispatch({type: ADD_SONG, payload: songInfo})
}

export const removeFavoriteSong = songId => dispatch => {
    dispatch({type: REMOVE_SONG, payload: songId})
}




