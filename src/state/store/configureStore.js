import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'
import { componentDataReducer } from '../reducers/componentDataReducer';

const persistConfig = {
  key: 'root',
  storage,
}

// Split the store in Reducers
const rootReducer = combineReducers({
  componentDataReducer
});

const pReducer = persistReducer(persistConfig, rootReducer);

//const store = createStore(pReducer, applyMiddleware(thunk));
//const persistor = persistStore(store);

export default () => {
  let store = createStore(pReducer, applyMiddleware(thunk))
  let persistor = persistStore(store)
  return { store, persistor }
}

// Declare and expose the Store
//export { persistor, store }; // The second argument is for Redux tools like Redux Thunk
